function binarySearch(sortedArray, key){
    // sortedArray = sortedArray.sort()
    let start = 0;
    let end = sortedArray.length - 1;
    let temp

    while (start <= end) {
        let middle = Math.floor((start + end) / 2);

        if (sortedArray[middle] === key) {
            // index ditemukan.
            return middle;
        } else if (sortedArray[middle] < key) {
            // melanjutkan pencarian ke arah kanan index
            start = middle + 1;
        } else {
            // melanjutkan pencarian ke arah kiri index
            end = middle - 1;
        }
    }
	// posisi index dari data tidak ditemukan
    return -1;
}

console.log(binarySearch([1, 2, 3, 4, 5], 3))